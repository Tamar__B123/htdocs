<?php
class html_page {
        protected $title = "title example";
        protected $body = "body example";
        public function view(){
            echo "<html><head><title>$this->title</title></head><body><p style = 'font-size:$this->size ; color:$this->color'> $this->body</p></body></html>";
        }
        function __construct($title = "",$body = ""){
            if($title != ""){
                $this->title = $title;
            }
            if($body != ""){
                $this->body = $body;
            }
        }
    }
?>
<?php
class coloredhtml extends html_page {
        protected $color = 'red';
        public function __set($property,$value){
            if($property == 'color'){
                $colors = array('red','yellow','green');
                if(in_array($value,$colors)){
                    $this->color = $value;
                }
                else{
                    die ("Please select one of allowed colors");
                }
            }    
        }
    }
?>
<?php
class sizedhtml extends coloredhtml {
        protected $size = '12';
        public function __set($property,$value){
            parent::__set($property,$value);
            if($property == 'size'){
                $sizes = array(10,11,12,13,14,15,16,17,18,19,20,21,22,23,24);
                if(in_array($value,$sizes)){
                    $this->size = $value;
                }
                else{
                    die ("Please select one of allowed font-sizes");
                }
            }
        }
    }
?>